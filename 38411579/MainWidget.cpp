#include "MainWidget.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QTableView>


MainWidget::MainWidget (QWidget * parent)
            : QWidget  (parent)
            , isEmpty  {false}
            , emptyRow {-1}
{
    setWindowTitle (tr ("New model rows to be non-empty") );

    QVBoxLayout * layoutTop = new QVBoxLayout ();
    QPushButton * button    = new QPushButton (tr ("Add") );
    QTableView  * table     = new QTableView  ();
    QStandardItemModel * model = new QStandardItemModel ();
    ItemDelegate * delegate = new ItemDelegate ();
    table->setModel (model);
    table->setItemDelegate (delegate);
    layoutTop->addWidget (table);
    layoutTop->addWidget (button);
    this->setLayout (layoutTop);

    connect (button, & QPushButton::clicked, [=](){
        QList <QStandardItem *> row;
        row << new QStandardItem ()
            << new QStandardItem ();
        model->appendRow (row);
        table->edit (model->index (model->rowCount () - 1, 0) );
    });

    connect (delegate, & ItemDelegate::closeEditor, [=](){
        if (isEmpty) {
            model->removeRow (emptyRow);
            isEmpty  = false;
            emptyRow = -1;
        }
    });

    connect (delegate, & ItemDelegate::cellEdited, [=](const int row){
        isEmpty  = true;
        emptyRow = row;
    });
}
