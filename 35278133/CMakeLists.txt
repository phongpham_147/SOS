cmake_minimum_required (VERSION 2.8)

project (QGBI)

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native -mtune=native -std=c++11 -Wall -Wextra")

find_package (Qt5Widgets REQUIRED)

set (
    QGBI_SOURCES
    ${PROJECT_SOURCE_DIR}/main.cpp
    ${PROJECT_SOURCE_DIR}/MainWidget.cpp
)

set (
    QGBI_HEADERS
    ${PROJECT_SOURCE_DIR}/MainWidget.h
)

qt5_wrap_cpp (
    QGBI_HEADERS_MOC ${QGBI_HEADERS}
)


add_executable (
    qgbi
    ${QGBI_SOURCES}
    ${QGBI_HEADERS_MOC}
)
target_link_libraries (qgbi Qt5::Widgets)
