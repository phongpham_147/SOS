#ifndef QGBI_MAIN_WIDGET_H
#define QGBI_MAIN_WIDGET_H

#include <QWidget>

class MainWidget : public QWidget
{
Q_OBJECT
public:
    explicit MainWidget     (QWidget * parent = nullptr);
    ~MainWidget             () override = default;
};

#endif//QGBI_WIDGET_H
