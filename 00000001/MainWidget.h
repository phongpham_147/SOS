#ifndef QPA_MAIN_WIDGET_H
#define QPA_MAIN_WIDGET_H

#include <QWidget>
#include <QGraphicsView>
#include "BarWidget.h"

class MainWidget : public QWidget
{
Q_OBJECT
public:
    explicit MainWidget     (QWidget * parent = nullptr);
    ~MainWidget             () override = default;
    bool eventFilter        (QObject *, QEvent *) override;
private:
    QGraphicsView         * view;
    BarWidget             * bar;
};

#endif//QPA_MAIN_WIDGET_H
