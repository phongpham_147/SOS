#ifndef QQKPE_MAIN_WIDGET_H
#define QQKPE_MAIN_WIDGET_H

#include <QWidget>
#include <QKeyEvent>

class MainWidget : public QWidget
{
Q_OBJECT
public:
    explicit MainWidget     (QWidget * parent = nullptr);
    ~MainWidget             () override = default;
    void keyPressEvent      (QKeyEvent *) override;
};

#endif//QQKPE_WIDGET_H
