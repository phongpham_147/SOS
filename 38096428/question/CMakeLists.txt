cmake_minimum_required (VERSION 2.8)

project (QQKPE)

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wextra")

find_package (Qt5Widgets REQUIRED)
find_package (Qt5Quick   REQUIRED)

set (
    QQKPE_SOURCES
    ${PROJECT_SOURCE_DIR}/main.cpp
    ${PROJECT_SOURCE_DIR}/MainWidget.cpp
)

set (
    QQKPE_HEADERS
    ${PROJECT_SOURCE_DIR}/MainWidget.h
)

qt5_wrap_cpp (
    QQKPE_HEADERS_MOC ${QQKPE_HEADERS}
)

set (QQKPE_RESOURCES
    ${PROJECT_SOURCE_DIR}/resources/resources.qrc
)
qt5_add_resources (
    QQKPE_RESOURCES_RCC ${QQKPE_RESOURCES}
)

add_executable (
    qqkpe
    ${QQKPE_SOURCES}
    ${QQKPE_HEADERS_MOC}
    ${QQKPE_RESOURCES_RCC}
)

target_link_libraries (
    qqkpe
    Qt5::Widgets
    Qt5::Quick
)
