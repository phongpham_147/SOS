#ifndef QQKPE_MAIN_WIDGET_H
#define QQKPE_MAIN_WIDGET_H

#include <QWidget>
#include <QKeyEvent>
#include <QDebug>

class MainWidget : public QWidget
{
Q_OBJECT
public:
    explicit MainWidget     (QWidget * parent = nullptr);
    ~MainWidget             () override = default;
    void keyPressEvent      (QKeyEvent *) override;
public slots:
    void onKeyPressEventQuick (const int, const int);
};

#endif//QQKPE_WIDGET_H
