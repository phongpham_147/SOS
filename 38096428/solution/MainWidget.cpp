#include "MainWidget.h"
#include <QQuickView>
#include <QHBoxLayout>
#include <QDebug>
#include <QQuickItem>

MainWidget::MainWidget (QWidget * parent)
            : QWidget (parent)
{
    setWindowTitle (tr ("QtQuick key press event propagation") );

    QQuickView * view = new QQuickView ();
    view->setSource (QUrl ("qrc:///widget.qml") );
    view->setResizeMode (QQuickView::SizeRootObjectToView);
    QWidget * container = QWidget::createWindowContainer (view, this);
    QVBoxLayout * topLayout = new QVBoxLayout;
    topLayout->addWidget (container);
    setLayout (topLayout);

    connect (view->rootObject (), SIGNAL (keyEvent (int, int) ), this, SLOT (onKeyPressEventQuick (int, int) ) );
}

void MainWidget::keyPressEvent (QKeyEvent * event)
{
    qDebug () << __FUNCTION__ << event->key () << event->modifiers ();
}

void MainWidget::onKeyPressEventQuick (const int key, const int modifiers)
{
    QKeyEvent * event = new QKeyEvent (QEvent::KeyPress, key, static_cast <Qt::KeyboardModifiers> (modifiers) );
    QCoreApplication::postEvent (this, event);
}
