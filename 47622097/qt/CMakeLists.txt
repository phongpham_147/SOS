cmake_minimum_required  (VERSION 3.0)
project (RdQt)
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra")

find_package (Qt5Widgets)

set (
    rdqt_SOURCES
    ${PROJECT_SOURCE_DIR}/main.cpp
    ${PROJECT_SOURCE_DIR}/Widget.cpp
    ${PROJECT_SOURCE_DIR}/Worker.cpp
)

set (
    rdqt_HEADERS
    ${PROJECT_SOURCE_DIR}/Widget.h
    ${PROJECT_SOURCE_DIR}/Worker.h
)

qt5_wrap_cpp (
    rdqt_HEADERS_MOC ${rdqt_HEADERS}
)

add_executable (
    rdqt
    ${rdqt_SOURCES}
    ${rdqt_HEADERS_MOC}
)

target_link_libraries (
    rdqt
    Qt5::Widgets
    pthread
)
