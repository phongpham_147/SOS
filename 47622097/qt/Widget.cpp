#include "Widget.h"
#include <QDebug>
#include <QApplication>

Widget::Widget (const char * path, QWidget * parent)
        : QWidget       (parent)
        , thread        {new QThread}
        , semaphore     {new QSemaphore (1)}
        , worker        {new Worker (path, semaphore)}
{
    connect (this, & Widget::start, worker, & Worker::onStart);
    connect (worker, & Worker::ready, this, & Widget::onReady);
    connect (worker, & Worker::finish, [this]() {
        thread->quit ();
        /*QApplication::quit ();*/
    });

    worker->moveToThread (thread);
    thread->start ();

    emit start          ();
}

Widget::~Widget ()
{
    worker->deleteLater ();
    thread->deleteLater ();
}

void Widget::onReady (char /*c*/)
{
    /*qDebug ("%c", c);*/
    semaphore->release ();
}
