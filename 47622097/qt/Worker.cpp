#include "Worker.h"
#include <QDebug>
#include <unistd.h>
#include <fcntl.h>

Worker::Worker (const char * path, QSemaphore * semaphore)
        : QObject   ()
        , path      {path}
        , semaphore {semaphore}
{
}

void Worker::onStart ()
{
    int file = open (path, O_RDONLY);
    char b;

    while (read (file, & b, 1) > 0) {
        semaphore->acquire ();
        emit ready (b);
    }
    qDebug () << strerror (errno) << (fcntl (file, F_GETFL) /*& O_NONBLOCK*/);

    emit finish ();
}
