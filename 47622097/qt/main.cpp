#include <QApplication>
#include "Widget.h"

int main (int argc, char * argv [])
{
    QApplication application (argc, argv);

    if (argc > 1) {
        Widget widget (argv [1]);
        widget.show ();

        return application.exec ();
    }

    return EXIT_FAILURE;
}
