#ifndef READ_DATA_WIDGET_H
#define READ_DATA_WIDGET_H

#include <QWidget>
#include <QThread>
#include <QSemaphore>
#include "Worker.h"

//                            Flow
//
//          Widget                              Worker
//            +
//            | create thread
//            | create worker                     +
//            | move worker to thread             |
//            | start thread                      |
//            |                                   |
//            | start                     onStart |
//            |---------------------------------->|
//            |                                   |
//            | onReady                     ready |
//            |<----------------------------------|  .--<--.
//            |                 semaphore acquire |  |     |
//            | print data                        |  |     ^
//            |                                   |  v     |
//            | semaphore release                 |  |     |
//            |---------------------------------->|  `-->--
//            |                                   |
//            |                                   |
//            |                          finished |
//            |                                   |
//            | delete worker                     -
//            | detete thread
//            | quit application
//            -

class Widget : public QWidget
{
    Q_OBJECT

    public:
        explicit Widget                 (const char *, QWidget * parent = nullptr);
        virtual ~Widget                 ();

    signals:
        void start                      ();

    public slots:
        void onReady                    (char);

    private:
        QThread                       * thread;
        QSemaphore                    * semaphore;
        Worker                        * worker;
};

#endif//READ_DATA_WIDGET_H
