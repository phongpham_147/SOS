#ifndef READ_DATA_WORKER_H
#define READ_DATA_WORKER_H

#include <QObject>
#include <QSemaphore>

class Worker : public QObject
{
    Q_OBJECT

    public:
        explicit Worker                 (const char *, QSemaphore *);
        virtual ~Worker                 () = default;

    signals:
        void ready                      (char);
        void finish                     ();

    public slots:
        void onStart                    ();

    private:
        const char *                    path;
        QSemaphore                    * semaphore;
};

#endif//READ_DATA_WORKER_H
