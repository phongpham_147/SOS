#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusMessage>
#include <QDebug>
#include <QDateTime>
#include <cstdlib>

int main (int /*argc*/, char ** /*argv*/)
{
    //dbus-send --system --print-reply --type=method_call --dest='org.freedesktop.timedate1' '/org/freedesktop/timedate1' org.freedesktop.timedate1.SetTime int64:120000000 boolean:true boolean:false
    QDBusInterface dbInterface ("org.freedesktop.timedate1", "/org/freedesktop/timedate1", "org.freedesktop.timedate1", QDBusConnection::systemBus () );
    qDebug () << dbInterface.call ("SetTime", 120000000ll, true, false);

    return EXIT_SUCCESS;
}
