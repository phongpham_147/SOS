#include <cstdlib>
#include <cmath>
#include <iostream>

int main ()
{
    using namespace std;

    float       fa (0), fb (0);
    double      da (0), db (0);
    long double la (0), lb (0);

    cout << "Float:       " << (FP_ZERO == fpclassify (fa - fb) ) << endl;
    cout << "Double:      " << (FP_ZERO == fpclassify (da - db) ) << endl;
    cout << "Long double: " << (FP_ZERO == fpclassify (la - lb) ) << endl;
    cout << "Float:       " << (FP_ZERO == fpclassify (fa - 42) ) << endl;
    cout << "Double:      " << (FP_ZERO == fpclassify (da - 42) ) << endl;
    cout << "Long double: " << (FP_ZERO == fpclassify (la - 42) ) << endl;

    return EXIT_SUCCESS;
}
