#ifndef QT_NETWORK_ANALYZER_H
#define QT_NETWORK_ANALYZER_H

#include <QThread>
#include <QNetworkConfigurationManager>
#include <QNetworkSession>
#include <QDebug>

class NetworkAnalyzer : public QObject
{
    Q_OBJECT

    public:
        NetworkAnalyzer (QObject * parent = nullptr)
        {
            QNetworkConfigurationManager nwManager (this);
            for (;;QThread::sleep (1) ) {
                bool isConfigurationFound {false};
                for (auto & configuration : nwManager.allConfigurations (/*QNetworkConfiguration::Active*/) ) {
                    // Name depends on the environment: "Wired connection 1", "McDonaldsFreeWiFi", "Home", "eth0", "eth1", etc...
                    if (isConfigurationFound = (configuration.name () == "eth0") ) {
                        QNetworkSession session (configuration, this);
                        session.open ();
                        session.waitForOpened (/*timeout*/);

                        qDebug () << "Session info:";
                        qDebug () << "- usage: " << (session.isOpen () ? "Opened" : "Closed");
                        qDebug () << "- state: " << (session.state  () == QNetworkSession::Connected ? "Connected" : "Not connected");

                        break;
                    }
                }
                qDebug () << (isConfigurationFound ? "" : "Configuration not found.");
            }
        }
};

#endif//QT_NETWORK_ANALYZER_H
