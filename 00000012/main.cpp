#include <QApplication>
#include "MainWidget.h"

int main (int argc, char * argv [])
{
    QApplication application (argc, argv);
    application.setStyleSheet ("QWidget {color: #d5d5d5; background-color: #323232;}");
    MainWidget mainWidget;
    mainWidget.show ();
    return application.exec ();
}
