#ifndef QSFPM_MAIN_WIDGET_H
#define QSFPM_MAIN_WIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QTreeView>
#include <QTableView>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QVBoxLayout>


class MainWidget : public QWidget
{
Q_OBJECT
public:
    MainWidget (QWidget * parent = nullptr) : QWidget (parent)
    {
        QPushButton * button = new QPushButton (tr ("Toggle") );

        QTableView * table = new QTableView (this);

        QTreeView * tree = new QTreeView (this);
        QStandardItemModel * model = new QStandardItemModel (this);
        model->appendRow (new QStandardItem ("1") );
        model->appendRow (new QStandardItem ("2") );
        QSortFilterProxyModel * modelProxy = new QSortFilterProxyModel (this);
        modelProxy->setSourceModel (model);
        tree->setModel (modelProxy);
        tree->hide ();

        QWidget * widgetToggle = new QWidget (this);
        QVBoxLayout * layoutToggle = new QVBoxLayout ();
        layoutToggle->addWidget (table);
        layoutToggle->addWidget (tree);
        widgetToggle->setLayout (layoutToggle);

        QVBoxLayout * layoutTop = new QVBoxLayout ();
        layoutTop->addWidget (button);
        layoutTop->addWidget (widgetToggle);
        this->setLayout (layoutTop);

        connect (button, & QPushButton::clicked, [=](){
            modelProxy->invalidate ();
            tree->show ();
        });
    }
};

#endif//QSFPM_WIDGET_H
